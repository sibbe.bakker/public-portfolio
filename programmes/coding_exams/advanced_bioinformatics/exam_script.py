#! /usr/bin/env python3
"""Summarising the transmembrane regions of an TMHMM analysis file

author --- Sibbe Bakker (1069349)

date --- 2022-11-25

dependencies --- from python: re, os, subprocess.  from linux: grep

description of usage
    python3 transmembrane_regions.py input_file output_file
        - input_file, path to a file created by DeepTMHMM, with a fasta
            style header indicating the start of a record. The first line
            after the record being a protein sequence, and the secord line
            being a annotation string, containing 'I':inside, 'O' : outside
            and 'M' for transmembrane. The file must not contain empty lines
            and all the records must have these two lines.
        - output_file path to the file where the coordinates of the regions
            (1 indexing) should be printed. Each record will have a tsv
            indicating the start and end of each annotation. The records are
            split by '//'. Before the start of each record there are two
            comment lines. In these lines The sequence length and number of
            transmembrane proteins is printed.

note
        By default, this script counts all the transmembrane proteins. If the
    user wants to only count the transmembrane regions that are not at
    the carboxyl or amino end of the protein, this can be done by setting
    `consider_ends = False` in the main function.
"""

# import statements ---
import os
import re
import subprocess
import sys

# Whether to overwrite the output_file if it is already present.
OVERWRITE = True

# functions ---
# input and output -----


def write_to_disk(object_to_write: any, file_path,
                  overwrite: bool = True) -> None:
    """Writing the string of an object to disk.

    :param object_to_write: any: Python object that can be converted to a
        string, meaning that it is accepted by the `python str(x)`
        constructor.
    :param file_path: str: The file to which object needs to be written.
    :param overwrite: bool: Whether to overwrite the file, if file path
        already exist. If False, and the file exist, no writing occurs and
        no error is given.
    :return: None, the function writes a file on disk.

    dependencies --- os
    """
    existence_file = os.path.exists(file_path)
    if existence_file:
        if overwrite:
            with open(file_path, 'w') as f:
                f.write(str(object_to_write))
        else:
            # Not allowed to write:
            pass
    else:
        with open(file_path, 'w') as f:
            f.write(str(object_to_write))
    return None


def parse_tmhmm_into_records(input_file_path) -> dict:
    """Parsing a tmhmm file into a list of records.

    :param input_file_path: str: Path of the file produced by TMHMM that
        should be parsed. The file may not contain empty lines.
    :return: dict:  dictionary with the DeepTMHMM annotation (I, O, M) as
        values. Keys are the headers of the records, eg XP_001545066.1 | GLOB.
    """
    lines_list = []
    with open(input_file_path, 'r') as f:
        for line in f:
            line_clean = line.strip('\n')
            if line_clean == '':
                ValueError(f'{input_file_path} may not contain empty lines.')
            lines_list.append(line_clean)
    assert len(lines_list) % 3 == 0, \
        ValueError(f'file {input_file_path} has incomplete records')

    headers = [head.replace('>', '') for head in lines_list[::3]]
    annotations = lines_list[2::3]
    records = {head: value for head, value in zip(headers, annotations)}
    return records


def print_file_summary(output_file_length: int, tmhmm_records: dict,
                       tm_records: dict) -> None:
    """Printing a summary of the DeepTMHMM file to sdtout.

    :param output_file_length: int: The length of the output file.
    :param tmhmm_records: dict: dictionary with the DeepTMHMM annotation
        (I, O, M) as values. Keys are the headers of the records,
         eg XP_001545066.1 | TM.
    :param tm_records: dict Same structure as tmhmm_records, but only
        containing records of TM proteins.
    :return: None, this function only outputs to standard out.
    """
    out_lines = [f'There are {len(tm_records)} transmembrane proteins '
                 'in the input file', f'{len(tmhmm_records)} records parsed',
                 f"My output file contains {output_file_length} lines"]

    out_text = '\n'.join(out_lines)
    print(out_text, )  # ending without a newline character, as specified.


def report_regions(region_dict: dict, consider_ends: bool = True) -> tuple:
    """Reporting the regions of a DeepTMHMM region dictionary.

    :param region_dict: dict:  Regions for each record as values and the
        sequence id as the dictionary key. The values are a list of tuples,
        where each tuple has the annotation ('inside', 'outside', 'membrane')
        as strings in the first item and the start, end regions as the
        second and third items encoded as ints.
    :param consider_ends: bool: Whether to count the transmembrane regions at the
        carboxyl/amino ends of the protein. True by default.
    :return: tuple: 1) the string of the report. 2) the number of lines
        in the report as an int.

    dependencies --- count_transmembrane_regions()
    """
    report_sequence_list = []
    for record in region_dict:
        regions = region_dict[record]
        length = regions[-1][2] - regions[0][1]
        tmr_n = count_transmembrane_regions(regions, consider_ends)
        record_report = [f'# {record} Length: {length + 1}',
                         f"# {record} Number of predicted TMRs: {tmr_n}"]
        for region in regions:
            cells = [record, region[0],
                     str(region[1]), str(region[2])]
            record_report.append('\t'.join(cells))
        record_report.append('//')
        report_sequence_list.append('\n'.join(record_report))
    out_text = '\n'.join(report_sequence_list)
    line_count = out_text.count('\n')
    return out_text, line_count


# external programme control -----
def count_tm_proteins(file_path: str) -> int:
    """Using grep on a DeepTMHMM output file to count transmembrane proteins.

    :param: file_path: str: Path to a file produced the DeepTMHMM programme.
    :return: int: The number of transmembrane proteins in the `file_path`
        file.

    dependencies --- os, subprocess
    """
    assert os.path.exists(file_path), \
        FileNotFoundError(f"The given file: {file_path} is not found.")

    # Fetching only (-o) the fields with TM and counting (-c) them.
    regex = r"'|\sTM'"
    command = f"grep {regex} {file_path} -o -c"
    output = subprocess.run(command, shell=True, check=True,
                            stdout=subprocess.PIPE)
    # The number of matching records.
    number = int(output.stdout.decode())
    return number


# analysis ---

def filter_tmhmm(records: dict, record_type: str = "TM"):
    """Filtering the records of TMHMM based on a header keyword.

    :param records: dict: dictionary with the DeepTMHMM annotation (I, O, M)
        as values. Keys are the headers of the records, eg XP_001545066.1 | TM.
    :param record_type: st: Record type to filter the records on, can be any
        string that is present at the end of the TMHMM header field. For
        example: 'TM' or "GLOB". Set to "TM" by default.
    :return: dict: The records that contain the `record_type`:str at the end
        of their headers. Records are a dictionary with the DeepTMHMM
        annotation (I, O, M) as values. Keys are the headers of the records,
        eg XP_001545066.1 | TM.
    """
    # h is header, v is value
    filtered_records = {h: v for h, v in records.items() if
                        h.endswith(record_type)}
    return filtered_records


def extract_tmhmm_annotations(records: dict) -> dict:
    """Obtaining the coordinates for DeepTMHMM annotations

    :param records: dict: dictionary with the DeepTMHMM annotation (I, O, M) as
        values. Keys are the headers of the records, eg XP_001545066.1 | GLOB.
    :return: dict: Regions for each record as values and the sequence id as
        the dictionary key. The values are a list of tuples, where each tuple
        has the annotation ('inside', 'outside', 'membrane') as strings in the
        the first item and the start, end regions as the second and third items
        encoded as ints.
    """
    regions_dict = {}
    for record in records:
        name = record.split()[0]
        annotation = records[record]
        annotation_list = []
        start = 0
        last_symbol = annotation[0]
        # Extracting regions of each annotation except the last one.
        for index, symbol in enumerate(annotation[1:]):
            if last_symbol == symbol:
                continue
            if last_symbol != symbol:
                annotation_words = 'NA'
                if last_symbol == 'I':
                    annotation_words = "inside"
                if last_symbol == 'M':
                    annotation_words = "TMhelix"
                if last_symbol == 'O':
                    annotation_words = "outside"
                out_list = (annotation_words, start + 1, index + 1)
                annotation_list.append(out_list)
                start = index + 1
                last_symbol = annotation[index + 1]
        # extracting the last record also
        annotation_words = 'NA'
        if last_symbol == 'I':
            annotation_words = "inside"
        if last_symbol == 'M':
            annotation_words = "TMhelix"
        if last_symbol == 'O':
            annotation_words = "outside"
        out_list = (annotation_words, start + 1, len(annotation))
        annotation_list.append(out_list)
        regions_dict[name] = annotation_list
    return regions_dict


def count_transmembrane_regions(regions: list,
                                consider_ends: bool = True) -> int:
    """Counting the number of transmembrane regions in a region list.

    :param regions: list: list of tuples, where each tuple has the annotation
        ('inside', 'outside', 'membrane') as strings in the first item and
        the start, end regions as the second and third items encoded as ints.
    :param consider_ends: bool: Whether to count the first/last transmembrane
        region if this is present (if True), if False, the function will only
        count transmembrane regions that are _not_ on either end of the
        protein, meaning only the middle regions are counted.
    :return: int: The number of transmembrane regions.
    """
    tm_regions = 0
    annotations = [v[0] for v in regions]
    if consider_ends:
        for annotation in annotations:
            if annotation == 'TMhelix':
                tm_regions += 1
    else:
        for annotation in annotations[1:-1]:
            if annotation == 'TMhelix':
                tm_regions += 1
    return tm_regions


# main function ---
def main():
    # Getting the input/output arguments
    input_file = sys.argv[1]
    output_file = sys.argv[2]
    consider_ends = True  # See module docstring

    # Counting the number of transmembrane proteins.
    number = count_tm_proteins(input_file)

    # Parsing the DeepTMHMM file into a dictionary
    tmhmm_records = parse_tmhmm_into_records(input_file)
    tm_records = filter_tmhmm(tmhmm_records, "TM")  # TM proteins.
    tm_dict = extract_tmhmm_annotations(tm_records)

    # Reporting
    report_string, len_report = report_regions(tm_dict, consider_ends)
    write_to_disk(report_string, output_file)
    print_file_summary(len_report, tmhmm_records, tm_records)


if __name__ == "__main__":
    main()
