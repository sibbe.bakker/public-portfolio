# portfolio

Hello, I am Sibbe Bakker and I welcome you to my portfolio! This is all of the
important work I have made over the course of my biology/(bio)informatic
journey.

Some highlights:

* [The presentation I held at the aspar_kr conference](https://bookdown.org/sibbe_l_bakker/aspar_presentation/aspar_presentation.html).
* The [report I made during my advanced bioinformatics course](https://git.wur.nl/sibbe.bakker/portfolio/-/blob/main/texts/reports/master/advanced_bioinformatics/report.pdf?ref_type=heads).
* The [report I made during the bioinformation technology course](https://git.wur.nl/sibbe.bakker/portfolio/-/blob/main/texts/reports/master/bioinformation_technology/Sibbe-Bakker_p1_22_23.pdf?ref_type=heads).
* The [report](https://git.wur.nl/sibbe.bakker/portfolio/-/blob/main/texts/reports/master/algorithms_in_bioinformatics/HMMs.pdf?ref_type=heads) for algorithms in bioinformatics, where I made a HMM.

